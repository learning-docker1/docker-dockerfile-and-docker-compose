# Understanding and Improving the Wordpress docker-compose.yml file

When start this docker container:
```
docker-compose up -d
```

Open following URL (localhost:8080)[http://localhost:8080] in the browser and setup Wordpress.

After that shutdown the docker container:
```
docker-compose down
```

And start this same docker container again:
```
docker-compose up -d
```

When accessing same URL again Wordpress setup is started again, previous setup process is lost now, because previous database and worpress are gone.

Shutdown this container:
```
docker-compose down
```

Adding named volumes to both services and defining same network for both services solves the issues with data persistence and with connection between containers.

After than start docker container again:
```
docker-compose up
```

Now all changes done to this container will not be lost.
