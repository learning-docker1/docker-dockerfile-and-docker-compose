<?php
header("content-type: text");
$host = "db"; // the hostname "db" from our docker-compose.yml file
$username = "root"; // the root user
$pw = "my!!!root!!!pw"; // that's the password set as environment variable

$conn = new mysqli($host, $username, $pw);

if ($conn->connect_errno > 0) {
    echo $db->connect_error;
} else {
    echo "DB Connection successful\n\n";

    // read out the content
    $result = mysqli_query($conn, "SHOW DATABASES;");
    while( $row = mysqli_fetch_row($result)) {
        echo $row[0]."\n";
    }
}
