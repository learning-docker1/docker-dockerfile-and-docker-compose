# Sample development environment (PHP/Apache/MySQL) using docker-compose.yml

To start docker container run following command:
```
docker-compose up -d
```

Option `-d` stands for detach, container will be build and started on the background.

To see the logs for example of MySQL container execute following command:
```
docker-compose logs -f db
```

To see logs from all container execute following command:
```
docker-compose logs -f
```

Now following URL [localhost:8080](http://localhost:8080) is available in the browser. 

When check running containers with this command:
```
docker-compose ps
```

Both containers will be listed:
```
    Name                  Command               State          Ports        
----------------------------------------------------------------------------
myphpapp-app   docker-php-entrypoint apac ...   Up      0.0.0.0:8080->80/tcp
myphpapp-db    docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp
```

Following command:
```
docker-compose down
```

Will stop and remove both containers.
