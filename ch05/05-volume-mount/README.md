# Understanding data persistence in host-volume mounted directories step by step

To start docker container run following command:
```
docker-compose up -d
```

Check running containers with this command:
```
docker-compose ps
```

Both containers will be listed:
```
          Name                         Command               State           Ports       
------------------------------------------------------------------------------------------
05-volume-mount_dbclient_1   docker-entrypoint.sh mysql ...   Exit 1                      
myphpapp-db                  docker-entrypoint.sh mysqld      Up       3306/tcp, 33060/tcp
```

Lets run client container:
```
docker-compose run --rm dbclient
```

This will start `dbclient` container and will login to mysql command line tool.
Now it is possible to exeucte some SQL commands.
This will take `somedatabase` into use:
```sql
use somedatabase;
```

Trying to show tables:
```sql
show tables;
```

Since there is no tables in this database, nothing is shown.

Lets create new table:
```sql
CREATE TABLE mytable (id INT);
```

Now this table can seen:
```sql
show tables;
+------------------------+
| Tables_in_somedatabase |
+------------------------+
| mytable                |
+------------------------+
1 row in set (0.00 sec)
```

Exit from *mysql* command prompt by typing command: `exit` and stop and remove containers:
```
docker-compose stop
docker-compose rm
```

Now when rebuild and restart container:
```
docker-compose up -d
```

And reconnect to `dbclient`:
```
docker-compose run --rm dbclient
```

```sql
use somedatabase;
show tables;
Empty set (0.00 sec)
```

All tables are gone since they were stored inside container and as soon as container was remove all tables were destroyed too.

Exit from *mysql* prompt and stop and remove containers:
```
docker-compose stop
docker-compose rm
```

One way to make database data persisted host machine directory can be mount as volume:
```
    volumes: 
      - ./data:/var/lib/mysql
```

This mounts host machine's `./data` directory as container's `/var/lib/mysql` volume.

Now when restart docker container:
```
docker-compose up -d
```

New directory `data` will be automatically created on host machine's working directory.

Now when login to *mysql* command prompt:
```
docker-compose run --rm dbclient
```

And when now create table:
```sql
use somedatabase;
CREATE TABLE mytable (id INT);
show tables;
+------------------------+
| Tables_in_somedatabase |
+------------------------+
| mytable                |
+------------------------+
1 row in set (0.00 sec)
```

Exit prompt, stop and remove container:
```
docker-compose stop
docker-compose rm
```

Now after restart of the `db` container:
```
docker-compose up -d
```

and reconnecting to `dbclient` container:
```
docker-compose run --rm dbclient
```

Previously create table is there:
```sql
use somedatabase;
show tables;
+------------------------+
| Tables_in_somedatabase |
+------------------------+
| mytable                |
+------------------------+
1 row in set (0.01 sec)
```

Exit *mysql* prompt and clean up containers:
```
docker-compose stop
docker-compose rm
```
