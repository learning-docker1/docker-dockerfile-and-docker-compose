# Build own images with custom configuration using docker-compose

To start docker container run following command:
```
docker-compose up
```

This will build container from `docker-compose.yml` and `Dockerfile` files, will start this newly created container and attach to container's interactive shell.

Now following URL [localhost:8080](http://localhost:8080) is available in the browser. It will show information about PHP configuration, but *MySQLi* is not abled. For that changes have to be done to `Dockerfile`.

Press `Ctrl-C` to stop the container.

Adding following line to the end of `Dockerfile` will install and enable needed package:
```
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
```

Remove the old container:
```
docker-compose rm
```

And then rebuild and start container again:
```
docker-compose up --build
```

Now *MySQLi* module is active.

To clean up environment stop the container with `Ctrl-C` and remove the container:
```
docker-compose rm
```
