# How to use docker-compose to start and stop services

To start docker container run following command:
```
docker-compose up
```

This will build container from `docker-compose.yml` file, will start this newly created container and attach to container's interactive shell.

Now following URL [localhost:8080](http://localhost:8080) is available in the browser.

But now since host machine working directory is mounted as a volume of docker container any changes made to `index.php` file will be immediately visible.

Pressing `Ctrl-C` will exit interactive shell and stop the container.

Now can remove container:
```
docker-compose rm
```
