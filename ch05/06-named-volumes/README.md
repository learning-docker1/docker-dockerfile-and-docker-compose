# Understanding data persistence in named-volumes and data-sharing in containers

To start docker container run following command:
```
docker-compose up
```

This command will start docker container and will create new volume and will give it a name automatically.
Lets stop the container and remove it:
```
docker-compose rm
```

This will remove the container but the volume will still be there:
```
docker volume ls
```

This will list all volumes including one created for this container:
```
DRIVER              VOLUME NAME
local               06-named-volumes_my-vol
```

By defining the volume name in `docker-compose.yml` file can control the naming of used volume:
```
volumes: 
  my-vol:
    name: my-vol
```

Lets remove this volume first:
```
docker volume rm 06-named-volumes_my-vol
```

and create new volume by hand:
```
docker volume create --name my-vol
```

When now restart container:
```
docker-compose up -d
```

Newly create volume will be mounted by this new container.

When starting new docker container and mounting same volume:
```
docker run -v my-vol:/mydata --rm -it ubuntu /bin/bash
```

Database files can be accessed from this mounted value:
```
ls /mydata
```

Exit container's shell and stop containers:
```
docker-compose down
```

Remove the volume:
```
docker volume rm my-vol
```
## How to share data between containers

Create some volume:
```
docker volume create --name datastore1
```

Start container with mounting this volume:
```
docker run -v datastore1:/mydatastore --rm -it ubuntu /bin/bash
```

Create file in this container on mounted volume:
```
echo "hello datastore1" > /mydatastore/hello.txt
```

Lets open new terminal and mount same volume in different container:
```
docker run -v datastore1:/mydatastore1 --rm -it ubuntu /bin/bash
```

When check the content of text file located on `datastore1` volume:
```
cat /mydatastore1/hello.txt 
hello datastore1
```

It will show same content as in the other docker container.

Lets append some text to this file in second container:
```
echo "hello datastore2" >> /mydatastore/hello.txt
```

When switching back to the first container and checking content of the file:
```
cat /mydatastore/hello.txt
hello datastore1
hello datastore2
```

It will contain the entry added in the second container.

This way data can be shared between container. Though file locking doesn't work at this level, so synchronisation mechanism should be implemented separately.

Exit container and do some clean up:
```
docker volume rm datastore1
```
