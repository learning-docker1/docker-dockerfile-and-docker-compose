# The docker-compose.yml file explained line-by-line

To start docker container run following command:
```
docker-compose up
```

This will build container from `docker-compose.yml` and `Dockerfile` files, will start this newly created container and attach to container's interactive shell.

Now following URL [localhost:8080](http://localhost:8080) is available in the browser.

Pressing `Ctrl-C` will exit interactive shell and stop the container.

When starting container again with same command as above, docker will not build the container again since it's alreay built.
Any changes made to `index.php` file will not be visible since it was added into the container and to propagate those changes to docker container, container should be rebuild:
```
docker-compose up --build
```

This will tell docker to rebuild container and changes to `index.php` file will be available in newly built container.

Now can remove container:
```
docker-compose rm
```
