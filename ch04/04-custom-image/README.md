# Upload custom image to Docker-Hub image repository

Build the container
```
docker build -t mycurl .
```

Run this image:
```
docker run --name my-curl mycurl www.google.com
```

This will get and output content of www.google.com site.

Can list available images:
```
docker image ls
```

It should contain image of `mycurl`:
```
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
mycurl                               latest              20015adf3173        3 minutes ago       8.67MB
```

To be able to publish docker image to Docker-Hub have to login:
```
docker login
```

Need to tag image properly:
```
docker tag mycurl <username>/mycurl:latest
```

Now can push image to Docker-hub:
```
docker push <username>/mycurl:latest
```

Now can remove container and all images:
```
docker rm my-curl
docker rmi mycurl
```

Now to run docker image from Docker-Hub execute following command:
```
docker run <username>/mycurl www.google.com
```
