# Ship your web-application using Apache/PHP as docker image with a Dockerfile

Build the container
```
docker build -t myphpapp:apache .
```

Run this image:
```
docker run --name myphpapp-apache -p 8080:80 myphpapp:apache
```

Now following URL [localhost:8080](http://localhost:8080) is available in the browser.

Now can remove container and the image:
```
docker rm myphpapp-apache
docker rmi myphpapp:apache
```
