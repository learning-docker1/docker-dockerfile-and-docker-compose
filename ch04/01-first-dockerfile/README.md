# Write first Dockefile step-by-step explained for Beginners

Build the container
```
docker build -t myphpapp .
```

This will execute all steps defined in `Dockerfile`.

When execute command:
```
docker image ls
```

It will list all downloaded images and will have following entry:
```
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
myphpapp                             latest              36bd220ea4ab        2 minutes ago       398MB
```

Run this image:
```
docker run --name myphp-app myphpapp
```

To remove container and this image use following command:
```
docker rm myphp-app
docker rmi myphpapp
```

