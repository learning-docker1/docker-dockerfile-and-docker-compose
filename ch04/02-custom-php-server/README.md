# Use a custom PHP development server inside a container based on a Dockerfile

Build the container
```
docker build -t myphpapp:web .
```

Run this image:
```
docker run --name myphpapp-web -p 8080:8000 myphpapp:web
```

Now following URL [localhost:8080](http://localhost:8080) is available in the browser.

Now can remove container and the image:
```
docker rm myphpapp-web
docker rmi myphpapp:web
```
