# Creating a specific network in the docker-compose.yml file

To start container defined in the `docker-compose.yml` file:
```
docker-compose up
```

The following URL will be available in the browser [localhost:8080](http://localhost:8080/).

When check running docker containers:
```
docker ps
```

can see running docker image:
```
ONTAINER ID        IMAGE               COMMAND              CREATED              STATUS              PORTS                  NAMES
18853bb9b3bd        httpd:latest        "httpd-foreground"   About a minute ago   Up About a minute   0.0.0.0:8080->80/tcp   app1
```

When inspecting this container:
```
docker inspect app1
```

In `NetworkSettings` can find defined network and when checking available networks:
```
docker network ls
```

Can see network with same ID:
```
NETWORK ID          NAME                                      DRIVER              SCOPE
dd19c18184a1        02-network-with-docker-compose_app1_net   bridge              local
```

Stop running container with `Ctrl-C` and remove docker container:
```
docker-compose rm
```
