# Understanding Network Segregation in Docker and the Docker0 network

To check available docker networks execute following command:
```
docker network ls
```

This will lists defined docker networks.

The following command will remove all docker networks:
```
docker network prune
```

Or individual network can be removed with this command:
```
docker network rm <networkname>
```

Lets run apache HTTP server container:
```
docker run --rm --name my-webserver -d httpd
```

When expecting running docker container with following command:
```
docker inspect my-webserver
```

There are some networks configurations already defined. The `bridge` configuration contains following entry `"IPAddress": "172.17.0.2"`.
But when trying to access server with the defined ip address in the browser it fails. 

When try to access this container's apache service using `mycurl` docker created previously:
```
docker run luhtonen/mycurl my-webserver
```

It responds with an error:
```
curl: (6) Could not resolve host: my-webserver
```

So it cannot be called by container name, but when called with docker ip address:
```
docker run luhtonen/mycurl 172.17.0.2
```

It returns successful response:
```html
<html><body><h1>It works!</h1></body></html>
```

Stop `my-webserver` container:
```
docker stop my-webserver
```

Let's create simple network:
```
docker network create simple-network
```

When check docker network:
```
docker network ls
```

Can see that newly create network is `bridge` network:
```
NETWORK ID          NAME                DRIVER              SCOPE
eb84b818656f        simple-network      bridge              local
```

Start `my-webserver` container with defined network:
```
docker run --rm -d --name my-webserver --network simple-network httpd
```

Now run `mycurl` again with connection to `simple-network`:
```
docker run --rm --network simple-network luhtonen/mycurl my-webserver
```

This time it is working with container name:
```html
<html><body><h1>It works!</h1></body></html>
```

When inspect the container:
```
docker inspect my-webserver
```

`simple-network` will be difined with different IP address:
```
"IPAddress": "172.18.0.2",
```

But when trying to access this sevice by this new IP address without specifying network:
```
docker run --rm luhtonen/mycurl 172.18.0.2
```

It will not connect to the container running apache server, because this container is running on its own network and it is completely segregated from *Docker0*  network, which is default network used when no network is defined.

Stop the docker container:
```
docker stop my-webserver
```

And remove manually created docker network:
```
docker network rm simple-network
```
