# Using and defining networks in docker-compose.yml file for network segregation

Let start docker container:
```
docker-compose up
```

When accessing in the browser URL [localhost:8080](http://localhost:8080/) there will be log entries from `app1_1` container and when accessing URL [localhost:8080/app2](http://localhost:8080/app2) then there will be log entries from `app2_1`.
