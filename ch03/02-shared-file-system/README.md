# Running Docker containers with a shared host file system (volume mounting)

Run following command:
```
docker run --rm -v ${PWD}:/myvol ubuntu /bin/bash -c "ls -lha > /myvol/myfile.txt"
```

This command:
```
docker ps
```

shows that there is no container running, but
```
cat myfile.txt
```

will output root directory content of the container.

Option `-v` mounted local file system directory, in this particular example current working directory `${PWD}` to the container's `/myvol` directory.

## Compressing and uncompressing files with docker

This will prints RAR utility help message:
```
docker run --rm klutchell/rar
```

To compress file run following command:
```
docker run --rm -v ${PWD}:/files klutchell/rar a /files/myfile.rar /files/myfile.txt
```

This will compress `myfile.txt` into `myfile.rar` archive.

The extra `-w` option with directory tells docker to set working directory to specified directory
```
docker run --rm -v ${PWD}:/files -w /files klutchell/rar a myfile.rar myfile.txt
```
