# Running PHP Scripts with Volume Mounting in Docker Containers - Practical Example

## Example of creating PHP file and running it in docker container

Start docker container from `php` image:
```
docker run -it --rm --name my-running-script php:7.2-cli /bin/bash
```

This will create new container from `php` image with the name `my-running-script`, start this container and go to interactive shell.

Check that php command line tool:
```
php -m
```

This will list PHP modules intalled on the system.

Switch to user home directory:
```
cd
```

and create simple PHP script:
```
echo '<? echo "test text\n";' > index.php
```

Execute newly created script:
```
root@29b70c4f2296:~# php index.php
```

This will product following output:
```
test text
```

Exit the docker shell with `exit` command or by `Ctrl-D`. This will stop and remove this container including all scripts and other files created inside this container.

## Example of volume mounting

Start docker container with mounted volume:
```
docker run -it --rm -v ${PWD}:/myfiles --name my-running-script php:7.2-cli /bin/bash
```

This will create exactly the same container as previous one, but with extra mounted directory `/myflies`.
Change to this directory
```
cd myfiles
```

and create same PHP script:
```
echo '<? echo "test text\n";' > index.php
```

When cxecute this script script:
```
root@29b70c4f2296:~# php index.php
```

Should see same output as previous one:
```
test text
```

But after exiting this container `index.php` script will be in working directory.
And when rerun this container with same command:
```
docker run -it --rm -v ${PWD}:/myfiles --name my-running-script php:7.2-cli /bin/bash
```

It will be possible to run same script again without a need to create it again:
```
cd myfiles/
php index.php
```

Adding working directory flag `-w` to the docker run command:
```
docker run -it --rm -v ${PWD}:/myfiles -w /myfiles --name my-running-script php:7.2-cli /bin/bash
```

Interactive shell will be starting in this working directory:
```
pwd
/myfiles
```
