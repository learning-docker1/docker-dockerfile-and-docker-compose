# Running multiple Docker containers, detaching and attaching

Create and start first container:
```
docker run -it -d --rm --name linux1 ubuntu /bin/bash
```
This will create new container from `ubuntu` image with the name `linux1` and will start the container and detach from it (option `-d`). Aftet this container will be stopped it will be removed (options `--rm`).

Create and start second container:
```
docker run -it -d --rm --name linux2 ubuntu /bin/bash
```
This will do exactly the same as command above, but container name will be `linux2`.

To see both running containers execute following command:
```
docker ps
```
This will show both running containers:
```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
54bd1905858c        ubuntu              "/bin/bash"         6 seconds ago       Up 5 seconds                            linux2
d60025a71675        ubuntu              "/bin/bash"         4 minutes ago       Up 4 minutes                            linux1
```

To attach to the running container use following command:
```
docker attach linux1
```

Lets create a directory in this container:
```
mkdir mylinux1
```

The following command:
```
ls
```

will show all directories including newly create `mylinux1`.

Lets attach to the other container:
```
docker attach linux2
```

And check the content of this container's root directory:
```
ls
```

This will not include `mylinux1` directory. 2 containers have separate file system and don't share anything between each other by default.

`Ctrl-D` on second container will exit process and shutdown the container.
Now this command:
```
docker ps -a
```

will show only one container `linux1` which still running. Container `linux2` is removed right after it was shutdown.

After exiting from the first container both containers will be gone.
