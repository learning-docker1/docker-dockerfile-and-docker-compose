#!/bin/sh

# create container from ubuntu image with the name "linux2"
# and will start the container and detach from it (-d)
# after container will be stopped it will be removed (--rm)
docker run -it -d --rm --name linux2 ubuntu /bin/bash

