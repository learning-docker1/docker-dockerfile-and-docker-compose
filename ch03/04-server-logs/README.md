# How to access Server Logs in Docker and how to do Port Forwarding in Docker

Let's start Apache httpd server in detached mode:
```
docker run --name my-httpd -d httpd
```

Check running images:
```
docker ps
```

This will produce output like similar:
```
CONTAINER ID        IMAGE               COMMAND              CREATED             STATUS              PORTS               NAMES
bc4aa846795e        httpd               "httpd-foreground"   50 seconds ago      Up 48 seconds       80/tcp              my-httpd
```

If try to access this HTTP server in the browser with URL [localhost:80](http://localhost:80) it will return HTTP error `404` - page not found.

Attach to docker container interactive shell:
```
docker exec -it my-httpd /bin/bash
```

Install `curl` on the container:
```
apt-get update
apt-get install curl
```

Check URL locally on container with `curl`:
```
curl localhost:80
```

It should output this:
```html
<html><body><h1>It works!</h1></body></html>
```

Exit the container shell and check container's logs:
```
docker logs my-httpd
```

There is only single HTTP request found:
```
127.0.0.1 - - [24/Nov/2020:14:55:20 +0000] "GET / HTTP/1.1" 200 45
```

which was sent from within the container, but no entry for request from outside the container.

By inspecting container's configuration:
```
docker inspect my-httpd
```

One entry is particularly interesting:
```
 "PortBindings": {},
```

This tells that there is no port bindings in this container.

Lets remove the container:
```
docker rm -f my-httpd
```

Now start the container with port binding flag `-p`:
```
docker run --name my-httpd -d -p 8080:80 httpd
```

This will bind container's port `80` to host machine port `8080`:
```
> docker ps

CONTAINER ID        IMAGE               COMMAND              CREATED             STATUS              PORTS                  NAMES
820a1d57caf3        httpd               "httpd-foreground"   5 seconds ago       Up 4 seconds        0.0.0.0:8080->80/tcp   my-httpd
```

`PORTS` column shows the port binding defined. Now containers HTTP server can be access on [localhost:8080](http://localhost:8080).
And if inspect docker configuration:
```
docker inspect my-httpd
```

there will be port bindings:
```
"PortBindings": {
    "80/tcp": [
        {
            "HostIp": "",
            "HostPort": "8080"
        }
    ]
},
```

Let's remove this container:
```
docker rm -f my-httpd
```

And run container to serve `index.php` from host's working directory:
```
docker run --name my-php -d -p 8080:80 -v ${PWD}:/var/www/html php:7.2-apache
```

Now when open in the browser URL: [localhost:8080](http://localhost:8080) text from script can be seen.

And when edit this `index.php` script in host machine and reload the browser, the change from the script will be seen on the screen.

Stop and remove this sample container:
```
docker stop my-php
docker rm my-php
```
